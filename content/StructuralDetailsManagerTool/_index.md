+++
weight = 3
title = "Structural Details Manager Tool"
bookCollapseSection = true
+++

# Structural Details Manager Tool

These pages contain instructions on how to use the Structural Details Manager (SDM) tool that allows structural engineers to generate the structural details catalog.

Use the menu on the left to navigate the different sections.
