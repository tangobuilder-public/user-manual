# How to create a new project

To create a new project click "New Project" in the Quick access. You will be taken to a new page Create a New Project menu.

![Dashboard](/img/Dashboard.png)

First you need to name your project, this name could be anything that helps you identify it.
In the following image, we present the menu to create a new project with a detailed description.

![Create new project](/img/CreateNewProject.png)

When you create a new project you need to select what kind of output results you would like from Tango Builder. You can choose between three types of output: Basic advanced or professional:

- The Basic output includes a 3D structural layout in DXF format, and a summary of the calculates for the model.
- The advanced output also includes a finite element model. If you select this design a menu will open for you to choose the type of model of your preference: ETABS, SAP2000, or others.
- The Professional output also includes a complete report and structural drawings including foundations, details, and a comparison between several structural options for your building.

## Advanced settings (expanded)

![Advanced settings expanded](/img/AdvancedSettingsExpanded.png)
