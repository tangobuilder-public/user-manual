# How to get the proposed design

When you create your model it will be uploaded to the cloud so that TANGObuilder can analyze and design your structure. Once it is done you can see your results by clicking on the name of your project and you will access your workspace.
You can review your design in our workspace, modify it if needed, and download the 3D model in several formats, including a complete finite element model.

![Download](/img/HowToGetTheDesign.png)

In your workspace, you will find:

- Left menu: a menu where you can select the views and output options.
- Preview structure area: you can see a 3D model of your structure and also verify the location of structural elements at each floor layout.
- Preview elements area: if you select an element you will find the measurements and typical detail suggested by Tango Builder.
- Back to dashboard button
