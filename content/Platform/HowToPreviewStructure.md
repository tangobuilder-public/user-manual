# Preview structure

Once you enter the workspace you can see a 3D model of your structure. In the left menu, under the project name, you can select the type of view.

![Dashboard](/img/PreviewStructure1.png)

In the following figure is a preview of the project, in 3D.

![Dashboard](/img/PreviewStructure2.png)

You can also verify the location of structural elements at each floor layout. Choosing them in the previous menu. In the following figure is a preview of the project, in floor.

![Dashboard](/img/PreviewStructure3.png)

## Tools Menu

You can use the tool menu, to hide any of the structural elements generated and better visualize the solution for your building.
