# How to add types of walls

Friday supports the following different types of walls as inputs:

- None-Structural walls.
- Shear walls.
- Non-bearing walls.
- Load-bearing walls.
- Auto.

All walls drafted as lines in the `internal` and `external` layers will be considered `Auto`. `Auto` means that the type of walls will be selected automatically based on internal algorithms to obtain the best results.

If you want `Friday` to process some walls in your project as non-bearing you simple should add `nonbearingwall` layer in your `dxf` input file. After export your `dxf` file to `Friday` all lines drawn within `nonbearingwall` layer will be recognized as non-bearing walls.

![Non bearing wall](/img/NonBearingWalls.png)

If you want `Friday` to process some walls in your project as load-bearing you simple should add `loadbearingwall` layer in your `dxf` input file. After export your `dxf` file to `Friday` all lines drawn within `loadbearingwall` layer will be recognized as load-bearing walls.

![Load bearing wall](/img/LoadBearingWalls.png)

Example input file containing the three types of walls: [TypesOfWalls.dxf](/ref/TypesOfWalls.dxf)
