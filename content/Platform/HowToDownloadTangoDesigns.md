# How to download Tango designs

First you need to navigate to the project you want to Download. On the left side of the workspace you will find the name of your project with some options to select: The type of construction, the view you want to see (3D or any level floor) and the extension of the file you want to export. You can export your 3D model as a DXF file or as an E2K file, so that you can further use it in Autocad, Revit, Sketchup, or ETABS. In the following figure, you will find the table with the options of the outputs.

![Download](/img/Download.png)

**DXF files:** 3D Component layout. Columns and beams are represented with lines and section information is provided. These files can be opened by AutoCAD, Revit and sketchup.

**E2K file:** You can open this document with Etabs to verify the design suggested by Tango Builder. You can also export a document to be opened by sap2000.

**calculation-package file:** You will find the zip file with all data composed during project calculation. This package contains the lateral systems chosen for your building, and a comparison between these structural systems in terms of quantity of materials, carbon footprint, and seismic performance.
