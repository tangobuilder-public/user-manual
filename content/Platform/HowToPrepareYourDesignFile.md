# How to prepare your design file

TANGObuilder processes `dxf` files with entities on `specific layers`. The default unit is `meter`. It is recommended to start with a dxf file containing the architectural design in one layer for reference. This layer can have any name and won't be processed.

The first step is to identify the external perimeter of the structure with a closed polygon of lines in the `external` layer.

Next, we need to draw lines in the corresponding layers to define the position and length of structural elements: shear walls, beams, bearing walls, windows, and doors. For columns, it is only required to draw a circle, and only the center position is processed.

Here is the list of specific layer names: `beam`,`column`, `shearWall`, `loadbearingwall`, `nonbearingwall`, `door`, and `windows`.

The last step is to define the direction of the secondary beams, adding a single line in the layer `secondarybeamsdirection` inside the area where the secondary beams will be generated. For secondary beams, the length and exact position of the line don't matter. Add just one line, ensuring the midpoint falls inside the area.

{{< youtube 0WO9falWisI >}}
