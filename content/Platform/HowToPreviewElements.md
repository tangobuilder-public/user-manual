# How to preview elements

You can select any of the structural elements such as beams or columns to check the size suggested by Tango Builder, the material, and a typical detail, this is presented in the image below.

![Dashboard](/img/PreviewElements.png)
