+++
weight = 1
title = "Platform"
bookCollapseSection = true
+++

# Platform

These pages contain instructions on preparing the inputs and running the TANGOBuilder platform.

Use the menu on the left to navigate the different sections
