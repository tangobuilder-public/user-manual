# How to create a multi-layout building

Friday can process a multi-layout building design.

![MultilayoutExample](/img/MultilayoutExample.png)

To process a multi-layout design, follow the next steps:

Create the `dxf` files for each level. In this example, we will create two different layouts, one for level 1 (`green` layout) and the other one for level 2 (`blue` layout). Level 0 layout is used to design foundations and is not covered in this article.

![MultilayoutDesignFiles](/img/MultilayoutDesignFiles.png)

Go to the [platform](https://app.tangobuilder.com/) and create a new project by filing all the basics fields.

![PlatformSetupBasic](/img/PlatformSetupBasic.png)

Then in the advanced settings section, set the `dxf` design files in the correct order as shown in the following image:

![PlatformSetupAdvanced](/img/PlatformSetupAdvanced.png)

In this example, we set only the `blue` layout in level 2, the `green` layout will be set automatically in the other levels that are not defined (level 1 and level 0).

The `e2k` output should look like this:

![MultilayoutOutput](/img/MultilayoutOutput.png)

Example input files:

- [Blue.dxf](/ref/Blue.dxf)
- [Green.dxf](/ref/Green.dxf)
