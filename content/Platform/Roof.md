# How to add Roof

Friday can generate and export `E2K` hip and gable roofs.

![MultistoryExample](/img/RoofMultistoryExample.png)

To add a roof to a building follow the next steps:

Open the `dxf` design file you want to add the roof and create a new layer named `roof`.
In the `roof` layer create a `block` for each roof panel. See hip and gable roof examples below:

![HipRoof](/img/RoofHip.png)

![GableRoof](/img/RoofGable.png)

Each block should contain lines that form a closed polygon. One line should be `Dashed` to specify the lower edge of the roof panel.  
Each block should have a different name followed by an underscore and the slope in degrees, ie `panelA_20.5`. Note that lines of one block will be overlapped with other lines from other blocks.  
In the platform, in the `advanced settings` section, set the `dxf` design files in the correct order as shown in the following image:

![PlatformSetup](/img/RoofPlatformSetup.png)

The output e2k should look like this:

![Output](/img/RoofOutput.png)

Example input files:

- [design_base.dxf](/ref/RoofBase.dxf)
- [design_hip.dxf](/ref/RoofHip.dxf)
- [design_gable.dxf](/ref/RoofGable.dxf)
