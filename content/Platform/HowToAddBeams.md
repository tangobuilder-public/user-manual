# How to add beams

TANGOBuilder supports to add beams as input.

All lines drafted in the `beam` layer will be considered a beam.

![beams](/img/Beams.png)

Example input file containing a beam [beam.dxf](/ref/Beams.dxf)
