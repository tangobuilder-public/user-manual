# Structural Drawings

TANGOBuilder automatically generates the structural drawing dxf document containing the following types of plan views:

- Level: Containing Stem walls, different types of walls, headers, beams, posts, joists, and sheathing. **Note:** Also contains underlay walls of the upper level.
- Roof: Contains ridge beams and posts.

## Plan views arrangement

All plan views are drafted in the model space following the next arrangement:

![Non bearing wall](/img/PlanViewArrangement.png)

## Plan views layouts

For each plan view in the modelspace, there is one paperspace that contains the title block and one viewport with the plan view entities. These paperspaces follow the naming convention `S1XX`.

## Title block content

The platform automatically generates the title block containing the company, project, and page information.

### Static content

This content is contained in the title block definition and is constant in all projects. This content is defined in the [title block template](https://gitlab.com/tangobuilder/jarvis/structural-drawings-templates/-/blob/main/titleBlock.dxf) file.

### Variable content

#### Project location

This content is automatically added to the title block definition by Friday.

| Content Name | Content |
|--------------|-----------|
|Address | Address where the project is located |
|City | City where the project is located |
|State | State where the project is located |

#### Date

This content is automatically added to the title block definition by Friday.

| Content Name | Content |
|--------------|-----------|
|Date | Current date in format `MM/DD/YY` |

#### Page information

This content is automatically added to each page by Friday.

| Content Name | Description |
|--------------|-----------|
|Sheet Number| Page number with format `SXXX` |
|Sheet Name| Page name defined as `STRUCTURAL LEVEL {level} {type} PLAN` |

Where:

- `level` is the number of the level: -1, 0 , 1, 2, etc.
- `type` is the type of plan view: `Foundation`, `Floor`, `Ceiling`, and `Roof`.

## Details Callouts

Friday will automatically identify the required details based on the presence of structural elements and connections. Then, it will add all the callouts containing the detail id to the model space of the structural drawing file.

## Hold-downs

Hold-downs are added to the stem walls. Each end of the walls and wall intersection has one hold-down. For wall intersections, only one hold-down is added to the wall with an angle `a` closer to 0 degrees from a horizontal reference line.

![Non bearing wall](/img/HoldDowns.png)

## Friday detail manager integration

`Friday` automatically adds the callouts with the details IDs to the model space. Then uses an integrated `structural-details-manager` module to process the details as follows:

1. The details ID texts are parsed and converted to a page and slot numbers in the model space.

   ![DMSModelSpace](/img/DMSModelSpace.png)

2. The details are searched in the `structural-details` database and imported into a paper space page in the corresponding slot.

   ![DMSPaperSpace](/img/DMSPaperSpace.png)
