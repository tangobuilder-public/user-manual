# How to model doors and windows

If you want Friday to process the doors and windows in your project, you simply should add "doors" and "windows" layers in your `dxf` file. All lines drawn within `doors` and `windows` layers will be recognized as appropriate openings. If you add a line in a layer named `opening`, those openings will be floor to ceiling.

![Windows layer](/img/WindowsLayer.png)

![Doors layer](/img/DoorsLayer.png)

To set sill plate height and header height, you can use the appropriate fields in the `Opening Parameters` submenu of `Configure Options`.

![Configure options menu](/img/ConfigureOptionsMenu.png)

![Opening parameters menu](/img/OpeningParametersMenu.png)

## Cautions

To correct parsing of openings the following actions during preparation of dxfs should be avoided:

### 1. Openings without underlying wall

![Opening without underlying wall](/img/OpeningWithoutUnderlyingWall.png)

### 2. Openings on intersection of walls

![Opening on intersection of walls](/img/OpeningOnIntersectionOfWalls.png)

Example input file: [DoorsAndWindows.dxf](/ref/DoorsAndWindows.dxf)
