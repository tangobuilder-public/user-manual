# How to specify secondary beam directions

The secondary beam direction can be specified by adding lines in the `secondaryBeamsDirections` layer. These lines are later processed by the `MeshGenerator` to determine the direction of the secondary beams inside each areas. It's necessary to add just one horizontal or vertical line within each area, ensuring that the midpoint of the line falls inside the area.

`Friday` automatically identifies and generates areas by forming closed polygons. These polygons are defined by the edges, which consist of beams and walls.

In the following example, we can see how the directions of the secondary beams are specified by just drawing a single line for each area.

![Doors layer](/img/SecondaryBeamDirection.png)
