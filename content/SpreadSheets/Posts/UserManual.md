# User Manual

This user manual provides detailed instructions on how to utilize the features of this spreadsheet effectively.

## How to Use the Spreadsheet

- The spreadsheet can be found here: [post.xlsx](/ref/post.xlsx).
- Complete all cells highlighted in pink in the sheet tab “SampleCalc”.
- Do not change any other values or add columns to the tables.
- Print the “SampleCalc” sheet tab using a Portrait configuration.

## Inputs Parameters

### Element Description Input

- Location: Specify the location of the column using the intersection of the gridlines where is located. For example, A-1 means the column is located at the intersection of gridlines A and 1.
- Level: Defines the story where the element is located.
- Label: Denotes the tag used for the post or mark.

### Design Loads

- P [lb]: Maximum compression over post due to ASD combinations.

### Design Geometry Input

- Post Size: Add the nominal section of the post
- length[ft]: Enter the length of the post.
- b[in]: rectangular dimension 1. Calculated based on the selected post size
- a[in]: rectangular dimension 2. Calculated based on the selected post size
- Category: Based on the post size, will be either dimensional lumber or timber/post

### Material Properties Input

Input the relevant material properties:

- Species and Commercial Grade: Add the wood species and the grade.
- Fc: Compression parallel to Grain (psi). Taken from Table 4A, or 4D.
- Emin: Minimum modulus of Elasticity (psi). Take from Table 4A, or 4D.

### Wood Adjustment factors

- {{< katex >}}C_D{{< /katex >}} : Load duration factor. Recommended value: 1 for D+L combination. Refer to (2018 NDS, 2018) - Section 2.3.2 for more information.

- {{< katex >}}C_M{{< /katex >}} : Wet service factor. Recommended value: 1 for moisture content less than 19% for an extended time period. Refer to (2018 NDS, 2018) - Section 4.3.3 for more information.

- {{< katex >}}C_t{{< /katex >}} : Temperature factor. Recommended value: 1 for not exposed to temperatures larger than 150 F. Refer to (2018 NDS, 2018) - Section 4.3.4 for more information.

- {{< katex >}}C_F{{< /katex >}} : Size factor. Recommended value: 1 to be conservative. Refer to (2018 NDS, 2018) - Section 4.3.6 for more information.

- {{< katex >}}C_i{{< /katex >}} : Incising factor. Recommended value: 1 for not incised. Refer to (2018 NDS, 2018) - Table 4.3.8 for more information.

- {{< katex >}}C_T{{< /katex >}}:Buckling stiffness factor. Recommended value: 1 to be conservative.

- {{< katex >}}c{{< /katex >}}: c factor required to compute stability factor. Refer to (2018 NDS, 2018) - Section 3.7.1.5 for more information.

## Calculations

The calculations are performed according to Section 3.7 of NDS 2018.

## Output Results

If the DCR is less than 1.0, the status is PASS; otherwise, the status is FAIL.

### Slenderness

- {{< katex >}}DCR_{\text{Slenderness}} = \frac{(\frac{l_e}{d})}{50}{{< /katex >}}

### Compression capacity parallel to the grain

- {{< katex >}}DCR_{\text{buckling}} = \frac{P}{Pc}{{< /katex >}}

### Bearing capacity parallel to the grain

- {{< katex >}}DCR_{\text{support}} = \frac{P}{Pbearing}{{< /katex >}}

---------------------------------------------------------------------------

## References

2018 NDS. (2018). American Wood Council. https://awc.org/publications/2018-nds/

2018 NDS Supplement. (2021). American Wood Council. https://awc.org/publications/2018-nds-supplement/

ASCE 7-22. (2021). https://www.asce.org/publications-and-news/civil-engineering-source/article/2021/12/02/updated-asce-7-22-standard-now-available

Breyer, D. E., Cobeen, K. E., & Martin, Z. (2017). Design of Wood Structures ASD/LRFD Eighth Edition. https://shop.iccsafe.org/design-of-wood-structures-asd-lrfd-eighth-edition.html
