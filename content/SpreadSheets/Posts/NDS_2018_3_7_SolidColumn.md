# NDS 2018 3.7 Solid column

The scope of this module is to calculate the sawn lumber nominal column compression capacity.
The building code subsections required include:

- NDS 2018 Section 3.7 Solid Columns
- NDS 2018 Section 3.10 Design for Bearing
- NDS 2018 Section 4 Sawn Lumber.
- NDS 2018 Supplement Table 4B and 4D Adjustment Factors and Reference Design Values

## Assumptions and limitations

- ASD method
- Douglas Fir No 2 Grade
- Compression only with no eccentricity
- Compression and bearing parallel to the grain
- Column unbraced along entire height ie, interior column (posts)
- Column end condition pin-pin (effective length factor Ke = 1.0)
- Square or rectangular column without taper along the length of the section
- No notching in the section
- Column slenderness ratio KeL/d <= 50

## Design Codes

- AWC-NDS (2018), National Design Specification (NDS) for Wood Construction 2018 Edition. *American Wood Council, Leesburg, VA.*
- AWC-NDS (2021), National Design Specification (NDS) Supplement: Design Values for Wood Construction 2018 Edition. *American Wood Council, Leesburg, VA.*
- ASCE 7-22. (2021). https://www.asce.org/publications-and-news/civil-engineering-source/article/2021/12/02/updated-asce-7-22-standard-now-available

## Inputs parameters

### Design Geometry Input

- L =  length of the post in ft. This is usually the story height of the building.
- b[in]: Rectangular dimension 1.
- a[in]: Rectangular dimension 2.

### Material Properties Input

Take these parameters from NDS_2018/4_2_ReferenceDesignValues

### Compression parallel to grain reference design value

- {{< katex >}}F_c{{< /katex >}}: Compression parallel to Grain, recommended for Douglas Fir Larch = 850 psi (2018 NDS Supplement, 2021).

- Emin: Minimum modulus of Elasticity, recommended for Douglas Fir Larch = 510000 psi (2018 NDS Supplement, 2021).

### Wood adjustment factors

Take these parameters from: NDS_2018/4_3_AdjustmentOfReferenceDesignValues.md

- {{< katex >}}C_D {{< /katex >}} : Load duration factor. Recommended value: 1 for D+L combination. Refer to (2018 NDS, 2018) - Section 2.3.2 for more information.

- {{< katex >}}C_M {{< /katex >}} : Wet service factor. Recommended value: 1 for moisture content less than 19% for an extended time period. Refer to (2018 NDS, 2018) - Section 4.3.3 for more information.

- {{< katex >}}C_t {{< /katex >}} : Temperature factor. Recommended value: 1 for not exposed to temperatures larger than 150 F. Refer to (2018 NDS, 2018) - Section 4.3.4 for more information.

- {{< katex >}}C_F {{< /katex >}} : Size factor. Recommended value: 1 to be conservative. Refer to (2018 NDS, 2018) - Section 4.3.6 for more information.

- {{< katex >}}C_i {{< /katex >}} : Incising factor. Recommended value: 1 for not incised. Refer to (2018 NDS, 2018) - Table 4.3.8 for more information.

- {{< katex >}}C_T {{< /katex >}}:Buckling stiffness factor. Recommended value: 1 to be conservative because it is only required for 2x4s or smaller members of wood trusses.

- {{< katex >}}c {{< /katex >}}: c factor to compute stability factor. Refer to (2018 NDS, 2018) - Section 3.7.1.5 for more information.

## Calculations

### Calculate the sectional area

- {{< katex >}}A_g {{< /katex >}} = a * b

### Calculate  maximum slenderness

- {{< katex >}}{(\frac{l_e}{d})} {{< /katex >}} = min [L/min (a,b), 50]

### Column Stability factor

- {{< katex >}}CP = \left(\frac{{1+\frac{{F_{\text{cE}}}}{{F*_{\text{c}}}}}}{{2 \cdot c}}\right) - \sqrt{\left(\left(\frac{{1+\frac{{F_{\text{cE}}}}{{F*_{\text{c}}}}}}{{2 \cdot c}}\right)^2 - \frac{{\frac{{F_{\text{cE}}}}{{F*_{\text{c}}}}}}{{c}}\right)} {{< /katex >}}: column stability factor, (2018 NDS, 2018) - Eq. 3.7.1,

Where:

- {{< katex >}}F_{\text{cE}} = \frac{0.822 \cdot E'_{\text{min}}}{(\frac{l_e}{d})^2} {{< /katex >}}
- {{< katex >}}F*_{\text{c}} = F_{\text{c}} \cdot C_D \cdot C_M \cdot C_t \cdot C_F \cdot C_i  {{< /katex >}} [lb/in²] adjusted ASD compressive design value parallel to grain without Cp.

### Maximum axial bearing stress

- {{< katex >}}Fbearing_{\text{c}} = 0.75 F*_{\text{c}} {{< /katex >}}

### Adjusted compressive stress parallel to grain

- {{< katex >}}F'_{\text{c}} = F_{\text{c}} \cdot C_D \cdot C_M \cdot C_t \cdot C_F \cdot C_P \cdot C_i  {{< /katex >}} [lb/in²] adjusted ASD compressive design value parallel to grain.

### Maximum axial stress allowed for the column

- {{< katex >}}MaxAxialStress = min(Fbearing_{\text{c}},F'_{\text{c}}) {{< /katex >}}

### Compression Capacity

- {{< katex >}}Pc = {A_g} * {MaxAxialStress}  {{< /katex >}} [in²] actual compressive capacity parallel to grain on center.

## Output Results

- Compression Capacity

## References

2018 NDS. (2018). American Wood Council. https://awc.org/publications/2018-nds/

2018 NDS Supplement. (2021). American Wood Council. https://awc.org/publications/2018-nds-supplement/

ASCE 7-22. (2021). https://www.asce.org/publications-and-news/civil-engineering-source/article/2021/12/02/updated-asce-7-22-standard-now-available

Breyer, D. E., Cobeen, K. E., & Martin, Z. (2017). Design of Wood Structures ASD/LRFD Eighth Edition. https://shop.iccsafe.org/design-of-wood-structures-asd-lrfd-eighth-edition.html

## Validation

Several validations were performed:

- [Post Validation 2](/src/PostValidation.pdf) vs Example 7.3 and Example 7.7 (Breyer et al., 2017).
