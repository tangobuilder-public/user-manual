# NDS 2018 Section 3.10 Bending Member - Bearing

The scope of this module is to calculate the sawn lumber beam bearing capacity.
The building code subsections required include:

- NDS 2018 Section 3.2 Bending Members - General
- NDS 2018 Section 3.10 Design for Bearing
- NDS 2018 Section 4 Sawn Lumber
- NDS 2018 Supplement Table 4B and 4D Adjustment Factors and Reference Design Values

## Assumptions and limitations

- ASD method
- Douglas Fir only
- Simply supported beam
- Rectangular section, not notched or tapered
- Bearing perpendicular to grain
- Minimum bearing length 3"
- Beam stability requirements in  NDS 2018 Section 4.4.1.2 must be met
- Beam sizes limited to 6", 8", 10", 12", 14"

## Design Codes

- AWC-NDS (2018), National Design Specification (NDS) for Wood Construction 2018 Edition. *American Wood Council, Leesburg, VA.*
- AWC-NDS (2021), National Design Specification (NDS) Supplement: Design Values for Wood Construction 2018 Edition. *American Wood Council, Leesburg, VA.*

## Inputs parameters

### Design Geometry Input

- b[in]: Rectangular dimension 1.
- d[in]: Rectangular dimension 2.
- lbe [in]: Bearing length at each end, set minimum equals to 3.5"

### Material Properties Input

Take Fcp from [Reference Design Values](Calculations/NDS_2018_4_2_ReferenceDesignValues.md)

### Compression perpendicular to grain reference design value

- {{< katex >}}F_cp [psi]{{< /katex >}}: Compression perpendicular to the grain

### Wood adjustment factors

Take these parameters from [Adjustment Values](Calculations/NDS_2018_4_3_AdjustmentOfReferenceDesignValues.md)

- {{< katex >}}C_M {{< /katex >}} : Wet service factor. Recommended value: 1 for moisture content less than 19% for an extended time period. Refer to (2018 NDS, 2018) - Section 4.3.3 for more information.

- {{< katex >}}C_t {{< /katex >}} : Temperature factor. Recommended value: 1 for not exposed to temperatures larger than 150 F. Refer to (2018 NDS, 2018) - Section 4.3.4 for more information.

- {{< katex >}}C_i {{< /katex >}} : Incising factor. Recommended value: 1 for not incised. Refer to (2018 NDS, 2018) - Table 4.3.8 for more information.

- {{< katex >}}C_b {{< /katex >}} : Bearing area factor.  Refer to (2018 NDS, 2018) - Section 3.10.4
- {{< katex >}} C_b = \frac{L_{be} + 0.375}{L_{be}} {{< /katex >}}

## Calculations

### Adjusted bearing capacity

Adjusted perpendicular compression stress {{< katex >}} F'_{cp} [psi] {{< /katex >}} is calculated as below:

- {{< katex >}} F'_{cp} = F_cp \cdot C_M  \cdot C_t  \cdot C_i  \cdot C_b {{< /katex >}}

### Bearing Strength

Bearing Capacity {{< katex >}} B_n [kip] {{< /katex >}}

- {{< katex >}} B_{n} = F'_{cp} \cdot b \cdot L{be} {{< /katex >}}

## Output Results

- Bearing Strength [psi]

## References

2018 NDS. (2018). American Wood Council. https://awc.org/publications/2018-nds/

2018 NDS Supplement. (2021). American Wood Council. https://awc.org/publications/2018-nds-supplement/

Breyer, D. E., Cobeen, K. E., & Martin, Z. (2017). Design of Wood Structures ASD/LRFD Eighth Edition. https://shop.iccsafe.org/design-of-wood-structures-asd-lrfd-eighth-edition.html

## Validation

Several validations were performed:

- [Beam Validation 1](/ref/BeamValidation1.pdf) from Example 6.18 (Breyer et al., 2017).
