# NDS 2018 Section 3.3 Bending Member - Flexure

The scope of this module is to calculate the sawn lumber beam bending capacity.
The building code subsections required include:

- NDS 2018 Section 3.2 Bending Members - General
- NDS 2018 Section 3.3 Bending Members - Flexure
- NDS 2018 Section 4 Sawn Lumber
- NDS 2018 Supplement Table 4B and 4D Adjustment Factors and Reference Design Values

## Assumptions and limitations

- ASD method
- Douglas Fir only
- Rectangular section, not notched or tapered
- Beam stability requirements in  NDS 2018 Section 4.4.1.2 must be met
- Beam sizes limited to 6", 8", 10", 12", 14"

## Design Codes

- AWC-NDS (2018), National Design Specification (NDS) for Wood Construction 2018 Edition. *American Wood Council, Leesburg, VA.*
- AWC-NDS (2021), National Design Specification (NDS) Supplement: Design Values for Wood Construction 2018 Edition. *American Wood Council, Leesburg, VA.*

## Inputs parameters

### Design Geometry Input

- b[in]: Rectangular dimension 1.
- d[in]: Rectangular dimension 2.

### Material Properties Input

Take Fb from [Reference Design Values](Calculations/NDS_2018_4_2_ReferenceDesignValues.md)

### Bending reference design value

- {{< katex >}}F_b{{< /katex >}}: Bending design value

### Wood adjustment factors

Take these parameters from [Adjustment Values](Calculations/NDS_2018_4_3_AdjustmentOfReferenceDesignValues.md)

- {{< katex >}}C_D {{< /katex >}} : Load duration factor. Recommended value: 1 for D+L combination. Refer to (2018 NDS, 2018) - Section 2.3.2

- {{< katex >}}C_M {{< /katex >}} : Wet service factor. Recommended value: 1 for moisture content less than 19% for an extended time period. Refer to (2018 NDS, 2018) - Section 4.3.3

- {{< katex >}}C_t {{< /katex >}} : Temperature factor. Recommended value: 1 for not exposed to temperatures larger than 150 F. Refer to (2018 NDS, 2018) - Section 4.3.4

- {{< katex >}}C_L {{< /katex >}} : Beam Stability Factor. Assumed value:1 since the beam meets the requirements of NDS 2018 Section 4.4.1.2. Refer to (2018 NDS, 2018) - Section 3.3.3.8

- {{< katex >}}C_F {{< /katex >}} : Size factor. Recommended value: 1 to be conservative. Refer to (2018 NDS, 2018) - Section 4.3.6

- {{< katex >}}C_fu {{< /katex >}} : Flat-use factor. Assumed value:1. Refer to (2018 NDS, 2018) - Section 4.3.7

- {{< katex >}}C_i {{< /katex >}} : Incising factor. Recommended value: 1 for not incised. Refer to (2018 NDS, 2018) - Table 4.3.8

- {{< katex >}}C_r {{< /katex >}}: Repetitive member factor. Recommended value: 1 to be conservative. Refer to (2018 NDS, 2018) - Section 4.3.9

## Calculations

### Calculate the section modulus

S [in3] is calculated a:

- {{< katex >}} S = \frac{b \cdot d^2}{6} {{< /katex >}}

### Adjusted bending capacity

Adjusted bending capacity {{< katex >}} f'_b [psi] {{< /katex >}} is calculated as:

- {{< katex >}} F'_{b} = F_b \cdot C_D \cdot C_M \cdot C_t \cdot C_L \cdot C_F \cdot C_fu \cdot C_i \cdot C_r {{< /katex >}}

### Bending Strength

The bending strength {{< katex >}} M_{n} [kip-ft] {{< /katex >}} is calculated as:

- {{< katex >}} M_{n} = F'_{b}  \cdot S{{< /katex >}}

## Output Results

- Bending Strength, [kip-ft]

## References

2018 NDS. (2018). American Wood Council. https://awc.org/publications/2018-nds/

2018 NDS Supplement. (2021). American Wood Council. https://awc.org/publications/2018-nds-supplement/

Breyer, D. E., Cobeen, K. E., & Martin, Z. (2017). [Design of Wood Structures ASD/LRFD Eighth Edition]. https://shop.iccsafe.org/design-of-wood-structures-asd-lrfd-eighth-edition.html

## Validation

The validation was performed using Example 6.18 from (Breyer et al., 2017).

- [Beam Validation 1](/ref/BeamValidation1.pdf)
