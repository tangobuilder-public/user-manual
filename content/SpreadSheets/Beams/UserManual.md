# User Manual

This user manual provides detailed instructions on how to utilize the features of this spreadsheet effectively.

## How to Use the Spreadsheet

- The spreadsheet can be found here: [beam.xlsx](/ref/beam.xlsx).
- Complete all cells highlighted in pink in the sheet tab “SampleCalc”
- Do not change any other values or add columns to the tables
- Print the “SampleCalc” sheet tab using a Portrait configuration

## Inputs Parameters

### Element Description Input

- Level: Defines the story where the element is located
- Label: Denotes the tag or mark used for the beam
- Type: Specify if this is a beam, joist, rafter, hip

### Design Loads

- M [kip-ft]: Maximum moment acting on the beam
- V [kip]: Maximum shear acting on the beam
- R [kip]: Maxium reaction acting on the beam

### Design Geometry Input

- length[ft]: Enter the length of the beam
- bn[in]: Enter the nominal width
- db[in]: Enter the nominal depth
- N: Enter the number of plies
- Lbe [in]: Enter the minimum bearing length for each end

### Material Properties Input

Take these values from [Reference Design Values](Calculations/NDS_2018_4_2_ReferenceDesignValues.md)

- Species and Commercial Grade: Add the wood species and the grade
- Fb: Bending value (psi)
- Emin: Minimum modulus of Elasticity (psi)
- Fv: Shear parallel to grain (psi)
- E: Modulus of elasticity (psi)
- Fcp: Compression perpendicular to grain (psi)

### Wood Adjustment factors

Take these parametersfrom [Adjustment Values](Calculations/NDS_2018_4_3_AdjustmentOfReferenceDesignValues.md). Multiply the adjustment factors and the reference design values to obtain the adjusted capacities below.

- {{< katex >}}C_D{{< /katex >}} : Load duration factor. Recommended value: 1 for D+L combination. Refer to (2018 NDS, 2018) - Section 2.3.2

- {{< katex >}}C_M{{< /katex >}} : Wet service factor. Recommended value: 1 for moisture content less than 19% for an extended time period. Refer to (2018 NDS, 2018) - Section 4.3.3

- {{< katex >}}C_t{{< /katex >}} : Temperature factor. Recommended value: 1 for not exposed to temperatures larger than 150 F. Refer to (2018 NDS, 2018) - Section 4.3.4

- {{< katex >}}C_F{{< /katex >}} : Size factor. Recommended value: 1 to be conservative. Refer to (2018 NDS, 2018) - Section 4.3.6

- {{< katex >}}C_fu {{< /katex >}} : Flat-use factor. Assumed value:1. Refer to (2018 NDS, 2018) - Section 4.3.7

- {{< katex >}}C_i{{< /katex >}} : Incising factor. Recommended value: 1 for not incised. Refer to (2018 NDS, 2018) - Table 4.3.8

- {{< katex >}}C_r {{< /katex >}}: Repetitive member factor. Recommended value: 1 to be conservative. Refer to (2018 NDS, 2018) - Section 4.3.9

- {{< katex >}}C_L {{< /katex >}} : Beam Stability Factor. Assumed value:1 since the beam meets the requirements of NDS 2018 Section 4.4.1.2. Refer to (2018 NDS, 2018) - Section 3.3.3.8

- {{< katex >}}C_b {{< /katex >}} : Bearing area factor.  Refer to (2018 NDS, 2018) - Section 3.10.4

- {{< katex >}} C_b = \frac{L_{be} + 0.375}{L_{be}} {{< /katex >}}

## Calculations

The calculations are performed according to these sections:

- [Section 3.3 Flexure](NDS_2018_3_3_BendingMembersFlexure.md)
- [Section 3.4 Shear](NDS_2018_3_4_BendingMembersShear.md)
- [Section 3.5 Deflection](NDS_2018_3_5_BendingMembersDeflection.md)
- [Section 3.10 Bearing](NDS_2018_3_10_BendingMembersBearing.md)

## Output Results

If the DCR for each limit state is less than 1.0, the status is PASS; otherwise, the status is FAIL.

### Bending Check

Bending strength {{< katex >}} M_{n} [kip-ft] {{< /katex >}}

{{< katex display >}} M_{n} = F'_{b}  \cdot S{{< /katex >}}

{{< katex display >}}DCR = \frac{M}{M_{n}} {{< /katex >}}

### Shear Check

Shear strength {{< katex >}} V_{n} [kip] {{< /katex >}}

{{< katex display>}} V_{n} = \frac{2}{3} F'_v  \cdot b  \cdot d  \cdot N{{< /katex >}}

{{< katex display>}}DCR = \frac{V}{V_{n}} {{< /katex >}}

### Deflection Check

Deflection demand {{< katex >}} Def_{a} [in] {{< /katex >}}

{{< katex display >}} Def_{a} = \frac{LTF \cdot 5  \cdot M \cdot L^2}{24E'  \cdot S  \cdot d}{{< /katex >}}

- Assume the long-term factor (LTF) equals 1.2 for conservative results

Deflection limit {{< katex >}} Def_{l} [in] {{< /katex >}}

{{< katex display>}} Def_{l} = min(0.75 in, L/240){{< /katex >}}

{{< katex display>}}DCR = \frac{Def_{a}}{Def_{l}} {{< /katex >}}

#### Bearing Check

Bearing Capacity {{< katex >}} B_n [kip] {{< /katex >}}

{{< katex display >}} B_{n} = F'_{cp}  \cdot b  \cdot L{be}  \cdot N{{< /katex >}}

{{< katex display >}}DCR = \frac{R}{B_{n}} {{< /katex >}}

---------------------------------------------------------------------------

## References

2018 NDS. (2018). American Wood Council. https://awc.org/publications/2018-nds/

2018 NDS Supplement. (2021). American Wood Council. https://awc.org/publications/2018-nds-supplement/

ASCE 7-22. (2021). https://www.asce.org/publications-and-news/civil-engineering-source/article/2021/12/02/updated-asce-7-22-standard-now-available

Breyer, D. E., Cobeen, K. E., & Martin, Z. (2017). Design of Wood Structures ASD/LRFD Eighth Edition. https://shop.iccsafe.org/design-of-wood-structures-asd-lrfd-eighth-edition.html

## Validation

The spreadsheet was validated using:

- [Beam Validation 1](/ref/BeamValidation1.pdf) from Example 6.18 (Breyer et al., 2017).
