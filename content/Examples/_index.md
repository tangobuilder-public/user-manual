+++
weight = 4
title = "Examples"
bookCollapseSection = true
+++

# Examples

These pages contain examples of input dxf files to test the platform.

Use the menu on the left to navigate the different sections.
