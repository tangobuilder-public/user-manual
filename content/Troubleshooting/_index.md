+++
weight = 5
title = "Troubleshooting"
bookCollapseSection = true
+++

# Troubleshooting

## Roof is not created correctly

Occasionally, when the roof is constructed, the roof layer may lack entities, causing the platform to overlook the roof during processing. This problem arises from how the roof block and its entities are created. To address this, it is essential to generate both the roof lines and the block within the designated `roof` layer simultaneously.
